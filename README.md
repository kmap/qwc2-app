KMap QGIS Web Client 2
======================

Forked from the upstream
["QGIS Web Client 2 Demo Application"](https://github.com/qgis/qwc2-demo-app)
repository.

## Requirements

Runtime dependencies (ArchLinux):

- `nodejs`: Javascript runtime

Additional build dependencies (ArchLinux):

- `make` (optional): utility to run targets
- `npm`: Package manager for web-app dependencies
- `yarn`: Package manager for overall project dependencies

## Building

0. Install dependencies:

```bash
make sync
```

1. Create a production build (in `prod/`):

```bash
make all
```

## Testing

0. Install dependencies (see: "[Building](#building)")
1. Start a development QGIS map server locally (on port 8000 by default):

```bash
qgis_mapserver -p /path/to/project.qgz
```

2. Run the local test server for the web-app (on port 8081 by default):

```bash
make run
```

## Package

*Note:* content is selected by [`manifest.txt`](./manifest.txt).

| Path                 | Install As                                |
|----------------------|-------------------------------------------|
|`├─dist/`             |                                           |
|`│ ├─nginx/`          |                                           |
|`│ │ └─qwc2-app.conf` |`/etc/nginx/sites-available/qwc2-app.conf` |
|`│ └─qwc2app.tmpfiles`|`/usr/lib/tmpfiles.d/qwc2app.conf`         |
|`├─prod/`             |                                           |
|`│ └─*`               |`/usr/share/kmap/qwc2/app/*`               |
|`└─LICENSE`           |`/usr/share/licenses/kmap-qwc2-app/LICENSE`|

## License

QWC2 is released under the terms of the [BSD license](https://github.com/qgis/qwc2-demo-app/blob/master/LICENSE).
