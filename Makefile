#! /usr/bin/env make -f

SHELL = /bin/sh
YARN = /usr/bin/yarn

.PHONY: help
help:
	@echo 'TARGETS:'
	@echo '  all ___________________ run the default target (prod)'
	@echo '  help __________________ show this help message'
	@echo '  prod __________________ build the webapp for production'
	@echo '  run ___________________ start a local test server for development'
	@echo '  sync __________________ download and install the project dependencies'

.PHONY: all
all: prod

.PHONY: sync
sync:
	@echo "INSTALLING DEPENDENCIES"
	$(YARN) install
	@echo "ok"

.PHONY: prod
prod:
	@echo "BUILDING (FOR PRODUCTION)"
	$(YARN) run prod
	@echo "ok"

.PHONY: run
run:
	@echo "RUNNING DEVELOPMENT SERVER"
	$(YARN) start
	@echo "done"
